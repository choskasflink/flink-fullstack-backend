# Flink Fullstack backend Test
## Installation
```
1. Clone Repo
2. Do "npm install"
3. Run with "npm start"
4. Test with "npm test"
```
### Content
- Live backend is [here](http://52.24.56.217:3001/)
- Complete CRUD with validations
- Jest and supertest library on testing
- Logrecords with winston that will store on /logs
- Gitlab CI/CD to EC2 on AWS
- Use of Docker
- Due to HTTP protocol problems the documentation by swagger is [here](https://flink-fullstack-backend.herokuapp.com/apiDocumentation/#/)