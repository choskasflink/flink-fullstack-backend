FROM node:12

RUN mkdir fullstackBackend

WORKDIR /fullstackBackend

ENV PATH /fullstackBackend/node_modules/.bin:$PATH

COPY . /fullstackBackend

RUN npm install

ENTRYPOINT ["npm", "start"]
