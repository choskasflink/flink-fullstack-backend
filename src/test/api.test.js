const app = require("../../index");
const request = require("supertest");
const { client } = require("../../config/dbConnect");

describe("Get Function", () => {
  it("should return status code 200", async () => {
    const response = await request(app).get("/api/getRegisters").send();
    expect(response.status).toBe(200);
  });
  it("should return data correctly", async () => {
    const response = await request(app).get("/api/getRegisters").send();
    const jsonData = JSON.parse(response.text);
    const getItem = jsonData.data.find((item) => item.symbol === "AAPL");
    expect(getItem).toMatchObject({
      _id: "61afe60f500b6e218f11c452",
      uuid: "9d6ee9d9-37bd-48c8-be46-b7fc0cdfa44e",
      companyName: "Apple",
      symbol: "AAPL",
      createdAt: "2021-12-07T22:54:07.915Z",
    });
  });
});

describe("Get only one company Function", () => {
  it("should return status code 200", async () => {
    const response = await request(app).get("/api/getRegisterByUUID?uuid=9d6ee9d9-37bd-48c8-be46-b7fc0cdfa44e").send();
    expect(response.status).toBe(200);
  });
  it("should return data correctly", async () => {
    const response = await request(app).get("/api/getRegisterByUUID?uuid=9d6ee9d9-37bd-48c8-be46-b7fc0cdfa44e").send();
    const jsonData = JSON.parse(response.text);
    expect(response.text.length).toBeGreaterThanOrEqual(30);
  });
});

describe("Post Function", () => {
  it("should return status code 200 and post data", async () => {
    const response = await request(app).post("/api/createRegister").send({
      companyName: "Microsoft",
      companyDescription: "Microsoft Corporation is an American multinational technology corporation which produces computer.",
      symbol: "msft",
    });
    expect(response.status).toBe(200);
  });
  it("should return error", async () => {
    const response = await request(app).post("/api/createRegister").send({
      companyName: "Maicrosoft",
      companyDescription: "12345678901234567890",
      symbol: "msftttt",
    });
    expect(response.status).toBe(400);
  });
});

describe("Update Function", () => {
  it("should return status code 200 and update data", async () => {
    const response = await request(app).put("/api/updateRegister").send({
      companyName: "Apple",
      companyDescription: "Apple Inc. is an American multinational technology company that specializes in consumer electronics.",
      symbol: "AAPL",
      uuid: "9d6ee9d9-37bd-48c8-be46-b7fc0cdfa44e",
    });
    expect(response.status).toBe(200);
  });
  it("should return error 400", async () => {
    const response = await request(app).put("/api/updateRegister").send({
      companyDescription: "Apple Inc. is an American multinational technology company that specializes in consumer electronics.",
      symbol: "AAPL",
      uuid: "9d6ee9d9-37bd-48c8-be46-b7fc0cdfa44e",
    });
    expect(response.status).toBe(400);
  });
});

describe("Delete Function", () => {
  it("should return error code 404", async () => {
    const response = await request(app).put("/api/deleteRegister/9d6ee9d9-37bd-48c8-be46-b7fc0cdfafff").send();
    expect(response.status).toBe(404);
  });
});