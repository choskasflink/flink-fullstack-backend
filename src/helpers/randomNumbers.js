const getRandomNumbers = () => {
    let string = '';
    for (let i = 0; i < 50; i++) {
        const number = Math.floor((Math.random() * 10000) + 1);
        string += number+",";
    }
    const listOfNumbers = string.substring(0, string.length - 1)
    return listOfNumbers;
}

module.exports = {
    getRandomNumbers
}