const stocks = require('stock-ticker-symbol');
const axios = require('axios');

const companyDataValidator = async (value) => {
    let isValid = true;
    let message = '';
    const polygonURL= `${process.env.POLYGON_API}?ticker=${value.symbol.toUpperCase()}&apiKey=${process.env.POLYGON_API_KEY}`;
    const polygonResponse = await axios.get(polygonURL);
    switch (true){
        case value.companyName.length > 50:
            isValid = false;
            message = 'Company name max lenght exceeded';
            break;
        case value.companyDescription.length > 100:
            isValid = false
            message = 'Company description max lenght exceeded'
            break;
        case value.symbol.length > 10:
            isValid = false
            message = 'Company symbol max lenght exceeded'
            break;
        case !polygonResponse.data.results:
            isValid = false
            message = 'Company symbol doesnt exist'
            break;
        case value.arrayCompanyValues.length > 50:
            isValid = false
            message = 'Market values max lenght exceeded'
            break;
    }
    return {
        success: isValid,
        message,
    }
}

module.exports = {
    companyDataValidator
}