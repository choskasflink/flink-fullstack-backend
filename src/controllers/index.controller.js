const Companies = require("../models/companies");
const { companyDataValidator } = require("../helpers/validators");
const { getRandomNumbers } = require("../helpers/randomNumbers");
const { v4: uuidv4 } = require('uuid');
const {logRecord} = require("../middleware/logRecord");

const getRegister = async (req, res) => {
    try {
        const companies = await Companies.find();
        res.status(200).json({success: true, message: 'Company get successfully', data: companies});
    } catch (error) {
        logRecord.error(`error: ${error}, requestFrom: ${req.originalUrl}`);
        res.status(500).send({success: false, message: error, data: []})
    }
};

const getRegisterByUUID = async (req, res) => {
    try {
        const {uuid} = req.query;
        if (!uuid) return res.status(400).json({success: false, message: "Missing data", data: []});
        const company = await Companies.findOne({uuid});
        res.status(200).json({success: true, message: 'Company found successfully', data: company});
    } catch (error) {
        logRecord.error(`error: ${error}, requestFrom: ${req.originalUrl}`);
        res.status(500).send({success: false, message: error, data: []})
    }
};

const createRegister = async (req, res) => {
    try {
        const {companyName, companyDescription, symbol} = req.body;
        if (!companyName || !companyDescription || !symbol) return res.status(400).json({success: false, message: "Missing data", data: []})
        const uuid = uuidv4();
        const randomNumbers = getRandomNumbers();
        const arrayCompanyValues = randomNumbers.split(',');
        const validateData = await companyDataValidator({companyName, companyDescription, symbol, arrayCompanyValues});
        if (!validateData.success) {
            res.status(400).json({success: false, message: validateData.message, data: []});
            return;
        }
        await Companies.create({uuid, companyName, companyDescription, symbol: symbol.toUpperCase(), marketValues: arrayCompanyValues});
        res.status(200).json({success: true, message: 'Company created successfully', data: []})
    } catch (error) {
        logRecord.error(`error: ${error}, requestFrom: ${req.originalUrl}`)
        res.status(500).send({success: false, message: error, data: []})
    }
};

const updateRegister = async (req, res) => {
    try {
        const {uuid, companyName, companyDescription, symbol} = req.body;
        if (!uuid || !companyName || !companyDescription || !symbol) return res.status(400).json({success: false, message: "Missing data", data: []})
        const randomNumbers = getRandomNumbers();
        const arrayCompanyValues = randomNumbers.split(',');
        const validateData = await companyDataValidator({companyName, companyDescription, symbol, arrayCompanyValues});
        if (!validateData.success) {
            res.status(400).json({success: false, message: validateData.message, data: []});
            return;
        }
        await Companies.findOneAndUpdate({uuid}, {companyName, companyDescription, symbol: symbol.toUpperCase(), marketValues: arrayCompanyValues});
        res.status(200).json({success: true, message: 'Company updated successfully', data: []})
    } catch (error) {
        logRecord.error(`error: ${error}, requestFrom: ${req.originalUrl}`)
        res.status(500).send({success: false, message: error, data: []})
    }
};

const deleteRegister = async (req, res) => {
    try {
        const {uuid} = req.params;
        if (!uuid) res.status(400).json({success: false, message: "Missing data", data: []})
        await Companies.findOneAndDelete({uuid});
        res.status(200).json({success: true, message: 'Company deleted successfully', data: []});
    } catch (error) {
        logRecord.error(`error: ${error}, requestFrom: ${req.originalUrl}`)
        res.status(500).send({success: false, message: error, data: []});
    }
};

module.exports = {
    getRegister,
    createRegister,
    updateRegister,
    deleteRegister,
    getRegisterByUUID,
}