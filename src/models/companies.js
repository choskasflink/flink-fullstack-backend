const { Schema, model } = require('mongoose')

const companiesSchema = new Schema(
  {
    uuid: {
        type: String,
        required: true,
    },
    companyName: {
      type: String,
      required: true,
    },
    companyDescription: {
      type: String,
      required: true,
    },
    symbol: {
      type: String,
      required: true,
    },
    marketValues: {
      type: Array,
      required: true,
    }
  },
 
  {
    timestamps: true,
    versionKey: false
  }
)

module.exports = model('Companies', companiesSchema)