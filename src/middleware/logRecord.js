const winston = require("winston");
require("winston-daily-rotate-file");

const logConfig = new winston.transports.DailyRotateFile({
  filename: "./logs/dash-%DATE%.log",
  datePattern: "YYYY-MM-DD",
  zippedArchive: true,
  maxSize: "20m",
});

const logRecord = winston.createLogger({
  transports: [
    logConfig,
    new winston.transports.Console({
      colorize: true,
    }),
  ],
});

module.exports = {
  logRecord,
};