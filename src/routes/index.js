const { Router } = require("express");
const {
  createRegister,
  updateRegister,
  deleteRegister,
  getRegister,
  getRegisterByUUID,
} = require("../controllers/index.controller");
const router = Router();

router.get("/getRegisters", getRegister);
router.get("/getRegisterByUUID", getRegisterByUUID);
router.post("/createRegister", createRegister);
router.put("/updateRegister", updateRegister);
router.delete("/deleteRegister/:uuid", deleteRegister);

module.exports = router;
