const mongoose = require('mongoose');
const { MongoClient } = require("mongodb");

const uri = process.env.MONGO_DATABASE;
const client = new MongoClient(uri);
const connectDB = async () => {
  try {
  await mongoose.connect(uri, {
    useUnifiedTopology: true,
    useNewUrlParser: true,
  })
  await client.connect();
} catch (error) {
  await  client.close()
}
}


module.exports = {connectDB, client}