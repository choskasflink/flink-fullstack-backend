require('dotenv').config()
const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const cors = require('cors');
const helmet = require('helmet');
const RateLimit = require('express-rate-limit');
const {connectDB} = require('./config/dbConnect');
const swaggerUi = require('swagger-ui-express'),
swaggerDocument = require('./swagger.json');
const app = express();


// Middlewares
app.use(helmet());
app.disable('x-powered-by');
app.use(bodyParser.json());
app.use(cors());
connectDB();

// Middlewares for DDoS and bruteforce attacks
const limiter = new RateLimit({
  windowMs: 15 * 60 * 1000, // 15 minutes
  max: 100,
  delayMs: 0,
});

app.use(limiter);

// Routes
app.use('/api', require('./src/routes/index'));
app.use(
    '/apiDocumentation',
    swaggerUi.serve, 
    swaggerUi.setup(swaggerDocument)
  );

// Static
app.use(express.static(path.join(__dirname, 'public')));

// Listen server
app.listen(process.env.PORT || 3001, () => {
  console.log('listening on port', process.env.PORT || 3001);
});

module.exports = app;